classdef PRNG <handle
    %% PRNG: Pseudo-Random Number Generator according to RSD distribution.
    % It is used to determine the DEGREE of the encoded block, i.e. the
    % number of source blocks (contributors) which will be XOR'ed to
    % generate the encoded block.


    properties(Access=private)
        seed    % Seed for PRNG
        K       % Number of blocks which compose the file
        delta
        c
        cdf     % RSD cumulative distribution function
    end

    properties(Constant)
        DEFAULT_DELTA=0.05;
        DEFAULT_C=0.2;
        DEFAULT_SEED=1234;
    end

    methods
        function obj = PRNG(K,delta,c,seed)
            %% Class constructor

            if nargin<4
                seed=obj.DEFAULT_SEED;
            end
            obj.seed=seed;
            rng(seed,'twister');
            obj.K=K;
            obj.delta=delta;
            obj.c=c;
            %obj.cdf=cumsum(RSD(K,delta,c)); %RSD cumulative distribution
            %obj.cdf=cumsum(Rho(K));
            fileid=fopen('+BATS_Code/simDegreeK1600M32m8.txt');
            pdf=fscanf(fileid,'%e ');
            cdf=cumsum(pdf);
            obj.cdf=cdf(1:floor(3200/K):end-mod(3200,K));%downsampling
            fclose(fileid);
            
        end

        function SetSeed(obj,seed)
            %% Set seed for PRNG
            obj.seed=seed;
            rng(seed,'twister');
        end

        function [d,indexes]=GetBlocks(obj)
            %% Returns the 'indexes' of a set of 'd' blocks
            % randomly sampled i = 1, ..., K-1 uniformly, where d is
            % sampled accordingly to the RSD.
            d=obj.Pick_d();
            indexes=randperm(obj.K,d);
        end

        function [d, indexes, Gmat]=GetGmatrix(obj,q,M)
            %% Returns the 'indexes' of a set of 'd' packets and a 'Gmat',
            % generator matrix for the outer code. The generator matrix is a
            % random matrix, whose elements are picked from a finite field GF(q)

            [d,indexes]=obj.GetBlocks();
            Gmat=gf(randi([0 q-1],d,M),log2(q));

        end

    end
    methods(Access=private)
        function d=Pick_d(obj)
            %% Pick a random d, following the RSD

            p=rand(); % Pick a random number between [0,1]
            % Find the first value in CDF, whose probability is greater
            % than p. The index of that value is the desired d.
            d=find(obj.cdf>p,1,'first');
            %d=randi([1 obj.K]);
        end

    end
end

function out = RSD(K,delta,c)
%% The Robust Soliton Distribution

%Get aux distributions
rho=Rho(K); %The Ideal Soliton Distribution
tau=Tau(K,c,delta);

Z=sum(rho)+sum(tau); %Normalizing term
out =(rho+tau)./Z;
end

function out=Rho(K)
%% The Ideal Soliton Distribution

out=zeros(1,K);%Preallocate
out(1)=1/K;
for d=2:K
    out(d)=1/(d*(d-1));
end
end

function out=Tau(K,c,delta)
%% Aux function for RSD

S=c*log(K/delta)*sqrt(K);%Expected number of degree-one checks
out=zeros(1,K);
for d=1:floor(K/S)
    if d < K/S -1
        out(d)=S/(K*d);
    elseif d == floor(K/S)
        out(d)=S/K*log(S/delta);
    end
end

end
