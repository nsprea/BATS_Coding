function [avg_deg,avg_rk,rk_defic] = BStats(batches)
%Compute statistics on batches
degrees=zeros(1,numel(batches));
ranks=zeros(1,numel(batches));
rk_defic=0;
for i=1:numel(batches)
    ranks(i)=batches(i).rank;
    degrees(i)=batches(i).degree;
    if ranks(i)<degrees(i)
        rk_defic=rk_defic+1;
    end
end
avg_deg=mean(degrees);
avg_rk=mean(ranks);
end

