clear;clc;
P=[1,0,1;0,0,1;0,1,0;1,1,1;];
%P=[1;0;0;1;];
num_packets=size(P,1);
num_symbols=size(P,2);

is_resolved=0;
i=1;
curr_rank=0;
%num_iter=1;
while ~is_resolved
    %num_iter=num_iter+1
    %Encode packets
    [EncVec,InfVec]=NetEncode(P,1);
    %Add encoding vector and encoded information to the last row of a
    %temporary matrix
    tmp(i,:)=[EncVec InfVec];

    %If the received data increases the rank of the received-data matrix
    if gfrank(tmp)>curr_rank
        %Then packet is innovative
        curr_rank=gfrank(tmp); %Update rank
        
        %Update received-data matrix and keep it in RREF form
        RcvdInfo=g2rref(tmp);
        
        %Check for finish
        if gfrank(RcvdInfo)>=num_packets
            is_resolved=1;
        end
        %Update counter (otherwise new-line is overwritten)
        i=i+1;
    end
end

%% Display results
disp('Original packet sequence: ');
disp(P');
disp('Received packet sequence: ');
disp(RcvdInfo(:,end-num_symbols+1:end)');

