clear;clc;
num_packets=8;
num_symbols=1024;
P=randi([0,1],num_packets,num_symbols);

num_encoded_pkts=num_packets*1.5;
disp('Encoding');
tic
%% Encode
for i=1:num_encoded_pkts
    [EncVec,InfVec]=NetEncode(P,1);
    ReceivedCoeff(i,:)=EncVec(:);
    ReceivedInfo(i,:)=InfVec(:);
end
toc

%% Intermediate node: Re-encode
% disp('Re-Encoding (relay node)');
% interval=2:5;
% tic
% [CoeffVec,InfVec] = NetReEncode(ReceivedCoeff(interval,:),ReceivedInfo(interval,:),1);
% toc
% ReceivedCoeff=[CoeffVec; ReceivedCoeff];
% ReceivedInfo=[InfVec; ReceivedInfo];

ReceivedInfo(2:4,:)=0;
ReceivedCoeff(2:4,:)=0;

tic
%% Decode
disp('Decoding');
i=1;
curr_rank=0;
for index=1:size(ReceivedCoeff,1)
    %Add encoding vector and encoded information to the last row of a
    %temporary matrix
    tmp(i,:)=[ReceivedCoeff(index,:) ReceivedInfo(index,:)];

    %If the received data increases the rank of the received-data matrix
    if gfrank(tmp)>curr_rank
        %Then packet is innovative
        curr_rank=gfrank(tmp); %Update rank
        
        %Update received-data matrix and keep it in RREF form
        RcvdInfo=g2rref(tmp);
        
        %Check for finish
        if gfrank(RcvdInfo)>=num_packets
            break;
        end
        %Update counter (otherwise new-line is overwritten)
        i=i+1;
    end
end
toc

%% Display results
% disp('Original packet sequence: ');
% disp(P');
% disp('Received packet sequence: ');
% disp(RcvdInfo(:,end-num_symbols+1:end)');
all(all(RcvdInfo(:,end-num_symbols+1:end)==P))
