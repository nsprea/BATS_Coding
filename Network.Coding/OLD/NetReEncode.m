function [CoeffVec,InfVec] = NetReEncode(CoeffMat,InfMat,Base)
%NET RE-ENCODE implements re-encoding at intermediate nodes.
%   Hyp:
%   -n is original number of packets
%   -r is number of received packets
%   -s is number of symbols per packet
%   INPUTS:
%   -'CoeffMat', (rxn) matrix of coefficients vectors
%   -'InfMat', (rxs) matrix of encoded packets
%   OUTPUTS:
%   -'CoeffVec', (1xn) vector of coefficients
%   -'InfVec', encoded (1xs) vector

[NewCoeff,InfVec]=NetEncode(InfMat,Base);
NewCoeff=gf(NewCoeff,Base); %Convert double array to GF(2^Base)
CoeffVec=NewCoeff*CoeffMat; %Check if multiplication if GF is required

CoeffVec=double(CoeffVec.x); %Convert GF array to double

end

