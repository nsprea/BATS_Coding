function [A] = PseudoInvertibleGFMatrix(m,n,p)
%% Compute mxn pseudo-invertible matrix in finite field GF(2).
% Implementation of algorithm described by:
% Dang, V.H. & Nguyen, T.D. Wireless Pers Commun (2017) 94: 455.
% https://doi.org/10.1007/s11277-015-3095-6

% Old implementation
% A1=randi([0,1],m,m);
% while det(A1)==0
%     A1=randi([0,1],m,m);
% end
% 
% A2=randi([0,1],m,n-m);
% M=A2*transpose(A2);
% while ~all(M(:)==0)
%     A2=randi([0,1],m,n-m);
%     M=A2*transpose(A2);
% end
% A=[A1,A2];

A1=randi([0,2^p-1],m,m);
U=triu(A1);
L=tril(A1);
for i=1:m
    U(i,i)=1;
    L(i,i)=1;
end

L=gf(L,p);
U=gf(U,p);

A1=L*U;

alpha=1;
Q=randi([0,2^p-1],m,(n-m)/2);
if mod(n-m,2)==0
   A2=[Q,alpha.*Q]; 
else
    C=zeros(m,1);
    A2=[Q,alpha.*Q,C];
end

A=[A1,A2];
%A=gf(A,1);
%A'*inv(A*A');

end

