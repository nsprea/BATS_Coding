classdef Decoder < handle
    %% Fountain-decoder
    
    properties(Access=private)
        K           % Number of blocks which compose the file
        delta
        c
        block_graph % Graph used for decoding
        
        
        prng        % Pseudo-random number generator used
                    % for checknode-symbol correlation
        decoded     % The decoded symbols
    end
    properties(GetAccess=public,SetAccess=private)
        is_done     % Bool: set to '1' when deconding is terminated
        N           % Number received blocks
    end
    
    properties(Constant,Access=private)
        MAX_REPETITIONS=2;
    end
    
    methods
        function obj = Decoder(K,blocksize,delta,c,max_repetitions,seed)
            %% Class constructor
            
            % Init number of received packets to '0'
            obj.N=0;
            
            %%Initialize graph
            obj.K=K;
            obj.block_graph=LT_Code.Graph(K);
            
            %%Initialize pseudo-random number generator with seed.
            obj.prng=LT_Code.PRNG(K,delta,c,seed);
            %obj.prng.SetSeed(seed);
            
            obj.is_done=0;
        end
        
        function ConsumeBlock(obj,block,blockseed)
            %% Consume a block of the file
            % Get indexes of srcblocks which it is connected to
            obj.prng.SetSeed(blockseed);
            [~,src_blocks]=obj.prng.GetBlocks();
            % Add block to the graph
            obj.is_done=obj.block_graph.add_block(block,src_blocks);
            % Increase received-blocks counter
            obj.N=obj.N+1;
        end
        
        function out=GetMessage(obj)
            %Return decoded message
            out=obj.block_graph.GetResolved();
        end
        
    end
end

