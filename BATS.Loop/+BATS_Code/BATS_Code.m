classdef BATS_Code < handle
    %% Batched Sparse code
    
    properties(SetAccess=private,GetAccess=public)
        T % Number of symbols
        K % Number of input packets
        M % Size of batches
        p1 % Order of galois field for outer code
        p2 % Order of galois field for inner code
        delta % Parameter for PRNG
        c % Parameter for PRNG
        seed
        ic_density %Inner code density
    end
    properties(Access=private)
        decoder %BP decoder
    end
    properties(Access=private,Constant)
        DEFAULT_MAX_BATCHES=10000;
        DEFAULT_OUTER_CODE_ORDER=8;
        DEFAULT_INNER_CODE_ORDER=1;
        DEFAULT_IC_DENSITY=0.4;
    end
    
    methods
        function obj = BATS_Code(M,max_batches,p1,p2,density,delta,c,seed)
            %% Class constructor
            
            %% Input arguments control
            narginchk(1,8)
            if nargin<2
                max_batches=obj.DEFAULT_MAX_BATCHES;
            end
            if nargin<3
                p1=obj.DEFAULT_OUTER_CODE_ORDER;
            end
            if nargin<4
                p2=obj.DEFAULT_INNER_CODE_ORDER;
            end
            if nargin<5
                density=obj.DEFAULT_IC_DENSITY;
            end
            if nargin<6
                delta=BATS_Code.PRNG.DEFAULT_DELTA;
            end
            if nargin<7
                c=BATS_Code.PRNG.DEFAULT_C;
            end           
            if nargin<8
                %seed=BATS_Code.PRNG.DEFAULT_SEED;
                seed=randi(65535);
            end
            
            %% Parameter initialization
            obj.M=M;
            obj.p1=p1;
            obj.p2=p2;
            obj.ic_density=density;
            obj.delta=delta;
            obj.c=c;
            obj.seed=seed;
            obj.decoder=0;
        end
        
        function [batch]=Encode(obj,in_pkts)
            %% Encode 'in_pkts' using BATS codes
            % Apply both outer and inner coding, according to the defined
            % parameters.
            
            %Compute size of input data
            [obj.T,obj.K]=size(in_pkts);
            
            %% Init the pseudo random number generator
            % Used to select batch degree, collaborator blocks and random
            % generator matrix
            prng=BATS_Code.PRNG(obj.K,obj.delta,obj.c,obj.seed);
            
            
            % Increase the seed of the batch
            obj.seed=obj.seed+1;
            prng.SetSeed(obj.seed);
            
            %% Outer coding
            % Get degree of batch, indexes of collaborator packets and
            % generator matrix
            [d,indexes,Gmat]=prng.GetGmatrix(2^obj.p1,obj.M);
            enc_data=in_pkts(:,indexes)*Gmat;
            
            %% Inner coding
            %Gnet=gf(randi([0 2^obj.p2-1],obj.M,obj.M),obj.p1);
            
%                 Gnet=BATS_Code.PseudoInvertibleGFMatrix(obj.M,obj.M,obj.p2);
%                 Gnet=gf(Gnet.x,obj.p1);
%                 enc_data=enc_data*Gnet;
%                 G=Gmat*Gnet;
            G=Gmat;
            
            batch=BATS_Code.Batch(obj.M,obj.seed,indexes,G,enc_data);

            
        end
        
        function is_done=Decode(obj,batch)
            %%Wrapper for decoder    
                if obj.decoder==0
                    obj.decoder=BATS_Code.BP_Decoder(obj.K);
                end
                is_done=obj.decoder.AddBatch(batch);
        end

        function message=GetMessage(obj)
                message=obj.decoder.decoded;
        end
        
        function [batch]=Recode(obj,batch,varargin)
            narginchk(2,3)
            if nargin==2
                %G=BATS_Code.PseudoInvertibleGFMatrix(obj.M,obj.M,obj.p2);
                G=BATS_Code.GFSprand(obj.M,obj.M,obj.ic_density,obj.p2,1);
                G=gf(G,obj.p1);
                batch=batch*G;
            elseif nargin==3
                %G=BATS_Code.PseudoInvertibleGFMatrix(obj.M,varargin{1},obj.p2);
                G=BATS_Code.GFSprand(obj.M,varargin{1},obj.ic_density,obj.p2,1);
                G=gf(G,obj.p1);
                batch=batch*G;
                obj.M=varargin{1};
            end
        end
        
    end
    
%     methods(Access=private)       
%         function [blocks,numblocks]=SplitFile(obj,fileID,blocksize)
%             %Split 'file' into blocks of size 'blocksize' and pads last block if
%             %dimension does not match.
%             
%             [fbytes,numbytes]=fread(fileID);
%             numblocks=ceil(numbytes/blocksize);%Number of blocks of blocksize
%             %Pad input file with zeros
%             padsize=blocksize*numblocks-numbytes;
%             fpadded=padarray(fbytes,padsize,0,'post');
%             %Split file
%             %blocks=reshape(fpadded,[numblocks,blocksize]);
%             blocks=reshape(fpadded,[blocksize,numblocks])';
%             
%             
%             blocks=uint8(blocks);
%         end
%     end
end

