function G = GFSprand(rows,cols,density,gforder,varargin)
%Generates uniformly random sparse matrix in GF(2^gforder)
narginchk(4,5)

%Create random sparse matrix
if nargin==4
    Gsparse=sprand(rows,cols,density);
elseif nargin==5
    Gsparse=sprand(rows,cols,density,varargin{1});
end

Greal=full(Gsparse);
G=floor(abs(Greal)*2^gforder);

%G=gf(Greal,2^gforder);

end

