classdef CheckNode < handle
    %% Class definition
    properties
        check
        src_nodes
    end
    
    methods
        function obj = CheckNode(src_nodes,check)
            obj.check=check;
            obj.src_nodes=src_nodes;
        end
    end
end


