function [CoeffVec,InfVec] = NetEncode(OriginalPkts,Base)
%NET ENCODE Apply network coding to OriginalPkts in GF(2^Base).
%   INPUTS:
%   -'OriginalPkts', a matrix where rows are packets and columns are symbols
%   -'Base', defines Galois Field in which function operates
%   OUTPUTS:
%   -vector of encoding coefficients 'CoeffVec'
%   -encoded packet 'InfVec'

num_packets=size(OriginalPkts,1);
sym_per_packet=size(OriginalPkts,2);

%Pick random coefficients in GF(2^Base)
CoeffVec=randi([0 2^Base-1],1,num_packets);

%% Compute encoded packet
OriginalPkts=gf(OriginalPkts,Base);
InfVec=zeros(1,sym_per_packet);
for i=1:num_packets
    %Multiply symbols by coefficients
    Product=CoeffVec(i).*OriginalPkts(i,:);

    %Convert GF array to double
    CurrPkt=double(Product.x);
 
    %Sum packets in GF(2^Base)
    InfVec=gfadd(InfVec,CurrPkt,2^Base);%For GF(2) is equivalent to bitxor
end
InfVec=double(InfVec);
end
