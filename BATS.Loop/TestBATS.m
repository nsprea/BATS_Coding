clear;
import BATS_Code.BATS_Code

%% Parameters
%============
T=2; %Number of symbols
K=512;   %Number of input packets
M=8;    %Batch size
p1=8;   %Order of GF for outer code
p2=1;   %Order of GF for inner code
ic_density=0.3; %Inner code density

num_links=1;
eps=0.0*ones(1,num_links); %Packet failure rate for each link
%-----------------------------------

fprintf('***** Simulation starts *****\n');
fprintf(' . Input data:\t\t%dX%d matrix,\n',T,K);
fprintf(' . Batch size M:\t%d\n',M);
fprintf(' . Outer code:\t\tGF(%d)\n',2^p1);
fprintf(' . Inner code:\t\tGF(%d)\n',2^p2);
fprintf(' . Number of links:\t%d\n',num_links);
fprintf(' . Packet failure rate:\t');
fprintf('%.02E -> ',eps(1:end-1)); fprintf('%.02E\n',eps(end));
fprintf('\n');

%% Input data
rng('shuffle','twister');
B=randi([0 2^p1-1],T,K);
%B(:,1)=[0;0];

%Define a BATS Code object
BC = BATS_Code.BATS_Code(M,1,p1,p2,ic_density);

decoded_flag=0;
iter=0;
fprintf('Transmitting... ');
fprintf('0000 batches');
tic
while decoded_flag==0
    iter=iter+1;
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b%4d batches', iter);
    %% Encoding
    enc_batch=BC.Encode(B);

    %% Propagation over channel
    for link=1:num_links
        E=ErasureMat(eps(link),M); %Erasures
        enc_batch=enc_batch*E;
        enc_batch=BC.Recode(enc_batch);%Recode
    end
    
    %% Decoding
    decoded_flag=BC.Decode(enc_batch);
end
fprintf('\b\b\b\b\b\b\b\b\b\b\b\bReceived. (%.2f seconds)\n',toc);
fprintf('Used %d batches.\n',iter);
message=BC.GetMessage();

%% Check matching with input data
if ~isempty(message)
    check=all(message(:)==B(:));
    if check~=1
        fprintf('Something gone wrong: mismatching with input data.\n');
    end
end

%% Save output
filename=['Pkts' num2str(K) 'Sym' num2str(T) 'M' num2str(M) , ...
    'Eps' num2str(eps(1)) 'Links' num2str(num_links) '.txt'];
fileid=fopen(filename,'a');
fprintf(fileid,'%d ',iter);
fclose(fileid);

fprintf('***** Simulation ended *****\n');
