classdef Batch < handle
    %BATCH
    properties
        seed_id
        degree
        indexes
        Gmat
        data
        M
        rank
    end
    
    methods
        function obj = Batch(M,seed_id,indexes,Gmat,data)
            if nargin>0 && nargin <6
                obj.M=M;
                obj.seed_id=seed_id;
                obj.degree=size(indexes,2);
                obj.indexes=indexes;
                obj.Gmat=Gmat;
                obj.data=data;
                obj.rank=rank(obj.Gmat);
            end
        end
        
        function tf=isDecodable(obj)
            tf= obj.rank==obj.degree && obj.degree~=0;
        end
        
        function r=mtimes(obj1,obj2)
            %%Overload matrix product operator
            data1=obj1;
            data2=obj2;
            G1=obj1;
            G2=obj2;
            
            if isa(obj1,'BATS_Code.Batch')
                data1=obj1.data;
                G1=obj1.Gmat;
                M=obj1.M;
                seed_id=obj1.seed_id;
                degree=obj1.degree;
                indexes=obj1.indexes;
            end
            if isa(obj2,'BATS_Code.Batch')
                data2=obj2.data;
                G2=obj2.Gmat;nex
                M=obj2.M;
                seed_id=obj2.seed_id;
                degree=obj2.degree;
                indexes=obj2.indexes;
            end
            
            data=data1*data2;
            Gmat=G1*G2;
            
            r=BATS_Code.Batch(M,seed_id,indexes,Gmat,data);
        end
        
        
        function [B,indexes]=Decode(obj)
            %Decode batch
            G=double(obj.Gmat.x);
            X=double(obj.data.x);
            
            i=1;
            successful=0;
            curr_rank=0;
            for col=1:size(G,2)
                tmp(:,i)=[G(:,col);X(:,col)];
                %Compare rank computed adding a new column with curr_rank
                new_rank=rank(gf(tmp,obj.Gmat.m));
                if new_rank>curr_rank
                    %If rank is increased, then packet is innovative
                    curr_rank=new_rank; %Update rank
                    
                    %Save coeff. to compute solution
                    Hd(:,i)=G(:,col); %Coefficients
                    Yd(:,i)=X(:,col); %Data
                    
                    %Check for finish
                    if curr_rank>=obj.degree
                        successful=1;
                        break;
                    end
                    %Update counter (otherwise new-line is overwritten)
                    i=i+1;
                end
            end
            
            if successful
                Yd=gf(Yd,obj.Gmat.m);
                Hd=gf(Hd,obj.Gmat.m);
                B=Yd/Hd; %Compute solution
            end
            
            %B=obj.data/obj.Gmat;
            indexes=obj.indexes;
            
            assert (size(indexes,2)==size(B,2))
            B=gf(B,obj.Gmat.m); %Convert to GF
            
        end
        
        function tf=RemoveContributors(obj,B,list)
            %%Remove pkts in list form contributors of current batch
            
            [ix,to_delete]=ismember(list,obj.indexes);
            if any(ix)
                %Position of indexes to delete from obj.indexes 
                to_delete=to_delete(ix); 
            
                B=double(B);
                B=B(:,any(B));%Remove possible empty columns

                assert (size(B,2)==size(list,2)) %Check dimensionality

                m=obj.Gmat.m;%GF order
                Gtmp=obj.Gmat.x; %Temporary G matrix
                Dtmp=obj.data; %Temporary data matrix

                %For each decoded packet in list
                for i=to_delete
                    %If packet is contributor for this batch
                    pkt_no=obj.indexes(i);
                    g=gf(Gtmp(i,:),m);
                    bj=B(:,list==pkt_no);
                    bj=gf(bj,m);
                    
                    Dtmp=Dtmp-bj*g; %Subtract from data matrix
                end
            
                obj.indexes(to_delete)=[];%Delete pkts from list of contributors
                Gtmp(to_delete,:)=[]; %Delete corresponding row in G matrix
                obj.degree=numel(obj.indexes);%Update degree

                %Convert to GF
                obj.Gmat=gf(Gtmp,m);
                obj.data=gf(Dtmp,m);

                obj.rank=rank(obj.Gmat);%Update rank
            end
        end
        
%         function obj=RemoveContributors(obj,B,list)
%             %%Remove pkts in list form contributors of current batch
%             
%             B=double(B);
%             
%             m=obj.Gmat.m;%GF order
%             Gtmp=obj.Gmat.x; %Temporary G matrix
%             Dtmp=obj.data; %Temporary data matrix
%             
%             %For each decoded packet in list
%             for j=list
%                 %If packet is contributor for this batch
%                 if ismember(j,obj.indexes)
%                    i=find(obj.indexes==j);
%                    obj.indexes(i)=[];%Delete pkt from list of contributors
%                    obj.degree=numel(obj.indexes);%Update degree
%                    
%                    g=gf(Gtmp(i,:),m);
%                    bj=B(:,j);
%                    bj=gf(bj,m);
%                    Gtmp(i,:)=[]; %Delete corresponding row in G matrix
%                    Dtmp=Dtmp-bj*g; %Subtract from data matrix
%                 end
%             end
%             %Convert to GF
%             obj.Gmat=gf(Gtmp,m);
%             obj.data=gf(Dtmp,m);
%         end
    end
end

