clear;
import LT_Code.LT_Code
%% Filenames
in_filename='lipsum128K.txt';
encoded_filename='encoded.txt';
out_filename='decoded.txt';

%% Parameters
%============
blocksize=128; %bytes
delta=0.5;
c=0.01;
rng('shuffle');
seed=rand();
% If the input file is composed of 'K' blocks, the fountain will transmit at
% most 'max_repetitions*K' blocks, i.e. the input file will be trasmitted
% entirely for 'max_repetitions' times
max_repetitions=2;

eps=0.2;%Packet failure rate


%% Open files
in_fileID=fopen(in_filename);
encoded_fileID=fopen(encoded_filename,'w+');
out_fileID=fopen(out_filename,'w+');

%% Encoding
fountain=LT_Code(blocksize,delta,c,seed,max_repetitions);

[encoded,blockseeds]=fountain.Encode(in_fileID,encoded_fileID);

%% Erasures
erasures=rand(1,fountain.K)>eps;
encoded(~erasures,:)=[];
blockseeds(~erasures)=[];

%% Decoding
tic
fountain.Decode(encoded,blockseeds,encoded_fileID,out_fileID);
endtime=toc;

%% Close files
fclose(in_fileID);
fclose(encoded_fileID);
fclose(out_fileID);

%% Log
fprintf('-----------------------------------------------\n');
fprintf('Input file:\t%s\n',in_filename);
fprintf('Output file:\t%s\n',out_filename);
fprintf('Block size:\t%d bytes\n',fountain.blocksize);
fprintf('delta:\t\t%1.2f\n',delta);
fprintf('c:\t\t%1.2f\n',c);
fprintf('Blocks:\t\t%d\n',fountain.K);
fprintf('Decoded blocks:\t%d\n',fountain.N);
fprintf('Code rate:\t%1.04f\n',fountain.code_rate);
fprintf('Redundancy:\t%1.04f\n',fountain.redundancy);
fprintf('Decoding time: \t%.04f seconds\n',endtime);

%% Compare original with decoded
diff_cmd=['diff ' out_filename ' ' in_filename];
[are_different,~]=unix(diff_cmd);
if are_different==0
    fprintf('[Test passed] Perfect matching with input file.\n');
else
    fprintf(['[Test failed] There is a mismatch with input file. ',...
        'Check if the mismatch is due to padding.\n']);
    %TODO: Resolve the mismatching due to padding
end
fprintf('-----------------------------------------------\n');

%% Remove useless files
unix(['rm ' encoded_filename]);
