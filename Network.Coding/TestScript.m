clear;
%% Parameters
%============
p=1; %order of GF(2^p)
eps=0.2; %Packet failure rate
delta=eps/(1-eps)+0.15;
num_symbols=1024;
num_packets=8;%K
num_encoded_pkts=14;%ceil((1+delta)*num_packets);%K'
%Note: if using PseudoInvertibleGFMatrix.m to generate encoding matrix, the
%quantity num_encoded_pkts - num_packets must be even.

%% Input data
B=randi([0,2^p-1],num_symbols,num_packets);
B=gf(B,p);

fprintf('-----Simulation starts-----\n');
fprintf('Number of symbols:\t%d\n',num_symbols);
fprintf('Number of packets:\t%d\n',num_packets);
fprintf('Encoded packets:\t%d\n',num_encoded_pkts);
fprintf('Finite field:\t\tGF(%d)\n',2^p);
fprintf('Packet failure rate: \t%.2f\n\n',eps);

fprintf('Encoding... ');
tic
%% Encode
%A=randi([0 2^p-1],num_packets,num_encoded_pkts); %Random choice of matrix
%Random matrix, but pseudo-invertible in GF:
A=PseudoInvertibleGFMatrix(num_packets,num_encoded_pkts,p);
A=gf(A,p);
X=B*A;
fprintf('Completed in %f seconds.\n',toc);
fprintf('  -Rank of encoding matrix A: %d\n',rank(A));

%% Erasures
%E=eye(num_encoded_pkts); %No erasure case
E=rand(1,num_encoded_pkts)>eps; %Create erasure matrix
fprintf('Erasures: %d/%d (%.1f%%)\n',nnz(~E),num_encoded_pkts, ...
    nnz(~E)/num_encoded_pkts*100);
E=diag(E);

X=X*E;
H=A*E;

tic
%% Decode
fprintf('Decoding... ');
H=double(H.x);
X=double(X.x);

i=1;
curr_rank=0;
successful=0;
for index=1:size(H,2)
    %Add encoding vector and encoded information to the last column of a
    %temporary matrix
    tmp(:,i)=[H(:,index); X(:,index)];

    %If the received data increases the rank of the received-data matrix
    if gfrank(tmp,2^p)>curr_rank
        %Then packet is innovative
        curr_rank=gfrank(tmp,2^p); %Update rank
        
        %Update received-data matrix
        Y=tmp;
        
        %Here we simply store the coefficients and data into separated
        %matrixes
        Hd(:,i)=H(:,index); %Coefficients
        Xd(:,i)=X(:,index); %Data
        
        %Check for finish
        if gfrank(Y, 2^p)>=num_packets
            successful=1;
            break;
        end
        %Update counter (otherwise new-line is overwritten)
        i=i+1;
    end
end

%% Display results
if successful
    % First method: Gauss-Jordan elimination
    Y=Y';
    Y=g2rref(Y);
    
    % Second method: matrix inversion
    Hd=gf(Hd,p);
    Xd=gf(Xd,p);
    Bd=Xd/Hd;
    
    fprintf('Successful (%f seconds).\n',toc);
    fprintf('  -Rank of H: %d\n', gfrank(H,2^p));
    fprintf('  -Rank of X: %d\n', gfrank(X,2^p));
    
    
    check1=all(all(Y(:,num_packets+1:end)'==B));
    check2=all(Bd(:)==B(:));
    
    fprintf('  -Check 1st decoding method (0/1): %d\n',check1);
    fprintf('  -Check 2nd decoding method (0/1): %d\n',check2);
    
else
    fprintf('Failed.\n');
    fprintf('  -Rank of H: %d\n', gfrank(H,2^p));
    fprintf('  -Rank of X: %d\n', gfrank(X,2^p));
end
fprintf('-----end of simulation-----\n');