clear;
import BATS_Code.BATS_Code

%% Parameters
%============
T=2; %Number of symbols
K=512;   %Number of input packets
M=32;    %Batch size
max_batches=1000; %`Infinite' batches
p1=8;   %Order of GF for outer code
p2=1;   %Order of GF for inner code
ic_density=0.8; %Inner code density

num_links=5;
eps=[0.4*ones(1,num_links)]; %Packet failure rate for each link
%-----------------------------------

fprintf('***** Simulation starts *****\n');
fprintf(' . Input data:\t\t%dX%d matrix,\n',T,K);
fprintf(' . Batch size M:\t%d\n',M);
fprintf(' . Encoded batches:\t%d\n',max_batches);
fprintf(' . Outer code:\t\tGF(%d)\n',2^p1);
fprintf(' . Inner code:\t\tGF(%d)\n',2^p2);
%fprintf(' . Packet failure rate:\t%.2f\n',eps);
fprintf('\n');

%% Input data
rng('shuffle','twister');
B=randi([0 2^p1-1],T,K);

%Define a BATS Code object
BC = BATS_Code.BATS_Code(M,max_batches,p1,p2,ic_density);

%% Encode
fprintf('----- Source encoding -----\n');
tic
batches=BC.Encode(B);
fprintf('Completed in %.2f seconds.\n',toc);

%% Batch stats
[avg_deg,avg_rk,rk_deficient]=BStats(batches);
fprintf('  Avg degree: %.2f, Avg rank:%.2f, Rank-deficient:%d/%d\n', ...
    avg_deg,avg_rk,rk_deficient, max_batches);

for link=1:num_links
    fprintf('----- %d -----\n',link);
    %% Erasures
    fprintf('Apply erasures... ');
    for i=1:numel(batches)
        E=ErasureMat(eps(link),M);
        batches(i)=batches(i)*E;
    end
    fprintf('Done.\n');

    %% Batch stats 
    [avg_deg,avg_rk,rk_deficient]=BStats(batches);
    fprintf('  Avg degree: %.2f, Avg rank:%.2f, Rank-deficient:%d/%d\n', ...
        avg_deg,avg_rk,rk_deficient, max_batches);

    %% Recoding
    tic
    fprintf('Recoding... ');
    if link==4
        M=16;
    elseif link==5
        M=8;
    end
    batches=BC.Recode(batches,M);
    fprintf('Completed in %.2f seconds.\n',toc);

    %% Batch stats 
%     [avg_deg,avg_rk,rk_deficient]=BStats(batches);
%     fprintf('  Avg degree: %.2f, Avg rank:%.2f, Rank-deficient:%d/%d\n', ...
%         avg_deg,avg_rk,rk_deficient, max_batches);
end

%% Decode
fprintf('----- Decoding -----\n');
tic
[message,received_batches]=BC.Decode(batches);
fprintf('Used %d batches. Completed in %.2f seconds.\n', ...
    received_batches,toc);

%% Check matching with input data
if ~isempty(message)
    check=all(message(:)==B(:));
    fprintf('Check matching with input data (0/1): %d\n',check);
end
fprintf('*****Simulation ended*****\n');