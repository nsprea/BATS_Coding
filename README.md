# Network coding for underwater data relaying
The goal of this project is to improve data-rates in underwater multihop networks, using BATS codes. Batched Sparse codes combine
* Fountain Coding
* Network Coding
to achieve better performance.
