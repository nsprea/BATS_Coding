function E = ErasureMat(eps,M)
%Generate MxM erasure matrix with erasure rate eps
E=rand(1,M)>eps;
E=diag(E);
end

