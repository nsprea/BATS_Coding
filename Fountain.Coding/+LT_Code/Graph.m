classdef Graph < handle
    %% Graph of relations between received blocks (checknodes)
    % and source blocks.
    % It automatically tries to resolve every time a new block is added.
    properties(Access=private)
        checks   % Vector of check nodes values
        num_blocks
        eliminated
        num_eliminated
    end
    
    methods
        function obj = Graph(num_blocks)
            %% Class constructor
            
            obj.checks=containers.Map('KeyType','single','valueType','any');
            obj.num_blocks=num_blocks;    
            obj.eliminated=cell(1,num_blocks);
            obj.num_eliminated=0;
        end
        
        function is_resolved=add_block(obj,data,nodes)
            %% Add node to the graph.
            % Return 1 if graph is resolved, 0 otherwise.
            is_resolved=0;
            % If block is pendant, immediately resolve it
            if size(nodes,2)==1
                to_eliminate=LT_Code.CStack();
                to_eliminate=obj.eliminate(nodes,data);
                
                while to_eliminate.size()>0 %&& ~is_resolved
                    ResolvableCheck=to_eliminate.pop();
                    new_to_delete=obj.eliminate(ResolvableCheck.src_nodes,ResolvableCheck.check);
                    if new_to_delete.size()>0
                        to_eliminate.extend(new_to_delete.content);
                    end
                    is_resolved=obj.num_eliminated>=obj.num_blocks;
                end
            else
                for node=nodes
                    if ~isempty(obj.eliminated{node})
                        nodes(nodes==node)=[];
                        data=bitxor(data,obj.eliminated{node});
                    end
                end
                
                if size(nodes,2)==1
                    is_resolved=obj.add_block(data,nodes);
                else
                    check=LT_Code.CheckNode(nodes,data);
                    for node=nodes
                        if ~isKey(obj.checks,node)
                            obj.checks(node)=LT_Code.CStack();
                        end
                            tmp=obj.checks(node);
                            tmp.push(check);
                            obj.checks(node)=tmp;
                    end
                end
            end
            is_resolved=obj.num_eliminated>=obj.num_blocks;
        end
            

        
        function out=GetResolved(obj)
            %% Return resolved source nodes
            out=cell2mat(obj.eliminated);
        end
        
    end
    
    methods(Access=private)
            function ResolvableChecks=eliminate(obj,node,data)
                ResolvableChecks=LT_Code.CStack();
                if isempty(node)
                    return;
                end
%                 if ~isempty(obj.eliminated{node})
%                     disp('Node has already been resolved.');
%                     return;
%                 end
                
                obj.num_eliminated=obj.num_eliminated+1;
                obj.eliminated{node}=data;
                if isKey(obj.checks, node)
                    others=obj.checks(node);
                    remove(obj.checks,node);
                else
                    others=LT_Code.CStack();
                end               
                
                while others.size>0
                    check=others.pop();
                    check.check=bitxor(check.check,data);
                    check.src_nodes(check.src_nodes==node)=[];
                    
                    if size(check.src_nodes,2)==1
                        ResolvableChecks.push(check);
                    end
                end
            end
            
%         function [to_resolve,data]=FindPendantNodes(obj)
%             %% Find the resolvable nodes.
%             % Find the pendant checknodes, i.e. connected to only one
%             % source node, using adjacency matrix.
%             % Return a list of corresponding srcnode and data.
%             
%             data=[];
%             to_resolve=[];
%             % For each checknode
%             for chknode=1:obj.N
%                 % Check the corresponing line in the adjacencies matrix:
%                 % if it contains only one element equal to '1' and the
%                 % other elements are '0', then the checknode is resolvable
%                 [~,srcnode,nonz]=find(obj.A(chknode,:),2);
%                 if size(nonz,2)==1
% %                 if nnz(obj.A(chknode,:))==1
%                     %[~,srcnode]=find(obj.A(chknode,:));
%                     to_resolve=[to_resolve; srcnode];
%                     data=[data; obj.checks(chknode,:)];
%                 end
%             end
%         end
%         
%         function RemoveNodeEdges(obj,srcnode)
%             %% Remove all edges of one source node
%             % i.e. remove all its connections to checknodes
%             obj.A(:,srcnode)=0;
%         end
%         
%         function ResolveNode(obj,data,srcnode)
%             %% Resolve 'srcnode' and pass message to connected checknodes
%             %1. Set source node = check node data
%             obj.resolved{srcnode}=data;
%             %2. Pass message to all check nodes which is connected to
%             nodes=find(obj.A(:,srcnode));
%             for i=nodes'
%                 obj.checks(i,:)=bitxor(obj.checks(i,:),data);
%             end
%             %3. Remove all edges from source node
%             obj.RemoveNodeEdges(srcnode);
%         end
        
    end
end
